/*
 * FireSql a detection and protection sql injection engine.
 *
 * Copyright (C) 2012  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 2012
 *
 */

#include <iostream>
#include <sstream>
#include "postgres_decoder.h"

void PostgresDecoder::decode(Connection &conn,boost::asio::mutable_buffers_1 buffer) 
{
	std::size_t bytes = boost::asio::buffer_size(buffer);
	unsigned char* payload = boost::asio::buffer_cast<unsigned char*>(buffer);

	// Postgres payload first byte type, 1-4 byte lenght of the payload
	if (payload[0] == '\x51') { //Simple query
		int query_size = (payload[1] <<  0) | (payload[2] <<8) | (payload[3] << 16) | (payload[4] << 24);
		const char * header = reinterpret_cast<const char*>(&payload[5]);
		std::string query(header,bytes - 5);
#ifdef DEBUG
        	std::cout << __FILE__ << ":"<< __FUNCTION__ << ":bytes:"<< bytes;
        	std::cout << " postgreslen:"<< query_size;
        	std::cout << std::endl;
		std::cout << query << std::endl;
#endif
                incTotalDecodeQueries();
                setIsQuery(true);
                setQuery(query);
	} else {
		setIsQuery(false);
	}
	return;
}

void PostgresDecoder::reject(Connection &conn,boost::asio::mutable_buffers_1 buffer,const std::string &query,int *bytes)
{
        unsigned char* payload = boost::asio::buffer_cast<unsigned char*>(buffer);
	int postgres_packet_len = 0;
	std::string message;
	std::ostringstream msg;

        msg << "Syntax error on:"<< query;
        message = msg.str();

        postgres_packet_len = 5;
        memcpy(payload ,"\x45\x00\x00\x00\x00",postgres_packet_len);

        memcpy(payload + 1 + postgres_packet_len, message.c_str(),message.length());
        postgres_packet_len += message.length() + 1;

        // Copy the header
        //memcpy(packet,"\x00\x00\x00\x01\xff",5);
        //memcpy(packet , &mysql_packet_len, 1);
        (*bytes) = postgres_packet_len ;
}
