/*
 * FireSql a detection and protection sql injection engine.
 *
 * Copyright (C) 2012  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 2012
 *
 */

#ifndef FIRESQL_DATABASE_DECODER_H
#define FIRESQL_DATABASE_DECODER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <boost/asio/buffer.hpp>
#include "connection.h"

class Connection;

class DatabaseDecoder
{
public:

	explicit DatabaseDecoder():total_decode_queries_(0),total_bogus_queries_(0),
		is_query_(false),query_() {}

	virtual void decode(Connection &conn,boost::asio::mutable_buffers_1 buffer) = 0;
	virtual void reject(Connection &conn,boost::asio::mutable_buffers_1 buffer,const std::string &query,int *bytes) = 0;
	virtual const char *getName() = 0;

	void incTotalDecodeQueries() { ++total_decode_queries_; }
	void incTotalBogusQueries() { ++total_bogus_queries_; }
	int32_t getTotalDecodeQueries() { return total_decode_queries_;}
	int32_t getTotalBogusQueries() { return total_bogus_queries_;}

	void setQuery(const std::string &query) { query_ = query; }
	const std::string &getQuery() { return query_;}
	bool isQuery() { return is_query_;}
	void setIsQuery(bool value) { is_query_ = value; }
private:
	int32_t total_decode_queries_;
	int32_t total_bogus_queries_;
	std::string query_;
	bool is_query_;
};

typedef std::shared_ptr<DatabaseDecoder> DatabaseDecoderPtr;

#endif // FIRESQL_DATABASE_DECODER_H

