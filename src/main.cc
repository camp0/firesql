/*
 * FireSql a detection and protection sql injection engine.
 *
 * Copyright (C) 2012  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 2012
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <csignal>
#include "action_manager.h"
#include "decode_manager.h"
#include "database_decoder.h"
#include "postgres_decoder.h"
#include "proxy.h"
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <fstream>

/* Factory of Database drivers supporte */
std::map<std::string, std::function<DatabaseDecoderPtr()>> driver_factory {
        { "mysql",        [](){return DatabaseDecoderPtr(new MysqlDecoder());}},
        { "postgres",     [](){return DatabaseDecoderPtr(new PostgresDecoder());}}
};

Proxy *proxy;
ActionManager *action_mng;

bool process_command_line(int argc, char **argv,
	std::string &config_file,std::string &driver_name,
	bool &is_daemon)
{
	namespace po = boost::program_options;

	po::options_description mandatory_ops("Mandatory arguments");
	mandatory_ops.add_options()
		("config,c",   po::value<std::string>(&config_file)->required(),
			"set the configure file  of the proxy.")
		("database,d",   po::value<std::string>(&driver_name)->required(),
			"set the database driver (mysql,postgres).")
        	;

	po::options_description optional_ops("Optional arguments");
	optional_ops.add_options()
		("daemon,D",     	"Daemonize the process.")
		("help,h",     	"show help")
		("version,v",   "show version string")
		;

	mandatory_ops.add(optional_ops);

	try
	{
		po::variables_map vm;
        	po::store(po::parse_command_line(argc, argv, mandatory_ops), vm);

		if (vm.count("daemon"))
		{
			is_daemon = true;
		}

        	if (vm.count("help"))
        	{
            		std::cout << "FireSql " VERSION << std::endl;
            		std::cout << mandatory_ops << std::endl;
            		return false;
        	}
        	if (vm.count("version"))
        	{
            		std::cout << "FireSql " VERSION << std::endl;
            		return false;
        	}


        	po::notify(vm);
    	}
	catch(boost::program_options::required_option& e)
    	{
            	std::cout << "FireSql " VERSION << std::endl;
        	std::cerr << "Error: " << e.what() << std::endl;
		std::cout << mandatory_ops << std::endl;
        	return false;
    	}
    	catch(...)
    	{	
            	std::cout << "FireSql " VERSION << std::endl;
        	std::cerr << "Unsupported option." << std::endl;
		std::cout << mandatory_ops << std::endl;
        	return false;
    	}


	return true;
}

void loadRulesFromFile(std::string filename,std::string action_str)
{
	std::ifstream f(filename);
	std::string regex_exp;
	ActionPtr action = ActionManager::getInstance()->getAction(action_str);

        if(f.is_open())
        {
        	while(f.good())
                {
                	getline(f,regex_exp);
                        if(regex_exp.size() >0)
                        {
                        	RuleManager::getInstance()->addRule(regex_exp,action);
                        }
		}
		f.close();
	}
} 

void signalHandler( int signum )
{
	proxy->stop();
	proxy->statistics();
	ActionManager::getInstance()->statistics();
	DecodeManager::destroyInstance();
	RuleManager::destroyInstance();
	ActionManager::destroyInstance();
	exit(signum);  
}

int main(int argc, char* argv[])
{
	std::string proxy_address;
	std::string database_address;
	std::string print_rules_file;
	std::string drop_rules_file;
	std::string close_rules_file;
	std::string reject_rules_file;
	std::string config_file;
	std::string driver_name;
	bool is_daemon = false;
	int proxy_port;
	int database_port;

	if(!process_command_line(argc,argv,config_file,driver_name,is_daemon))
	{
		return 1;
	}

	DatabaseDecoderPtr driver;

        try {
                driver = driver_factory[driver_name]();
        } catch (std::bad_function_call& e) {
                std::cout << PACKAGE ": Unknown database driver " << driver_name << std::endl;
                exit(-1);
        }

	DecodeManager::getInstance()->setDefaultDecoder(driver);

	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(config_file, pt);

        try
        {
		proxy_address = pt.get<std::string>("Server.proxy_address");
		proxy_port = pt.get<int>("Server.proxy_port");
		
		database_address = pt.get<std::string>("Server.database_address");
		database_port = pt.get<int>("Server.database_port");

		print_rules_file = pt.get<std::string>("Rules.print");
		close_rules_file = pt.get<std::string>("Rules.close");
		drop_rules_file = pt.get<std::string>("Rules.drop");
		reject_rules_file = pt.get<std::string>("Rules.reject");

        }
        catch(boost::program_options::required_option& e)
        {
                std::cout << "FireSql " VERSION << std::endl;
                std::cerr << "Error: " << e.what() << std::endl;
                return false;
        }
        catch(...)
        {
                std::cout << "FireSql " VERSION << std::endl;
                std::cerr << "Unsupported option." << std::endl;
                return false;
        }

	loadRulesFromFile(drop_rules_file,"drop");
	loadRulesFromFile(reject_rules_file,"reject");
	loadRulesFromFile(close_rules_file,"close");
	loadRulesFromFile(print_rules_file,"print");

    	signal(SIGINT, signalHandler);  

	if(is_daemon)
	{
		daemon(1,1);
	}

   	try
   	{
		proxy = new Proxy(proxy_address,proxy_port,database_address,database_port);
		
		proxy->start();
		proxy->run();
   	}
   	catch(std::exception& e)
   	{
      		std::cerr << "Error: " << e.what() << std::endl;
      		return 1;
   	}
	return 0;
}

