/*
 * FireSql a detection and protection sql injection engine.
 *
 * Copyright (C) 2012  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 2012
 *
 */

#ifndef FIRESQL_DECODE_MANAGER_H
#define FIRESQL_DECODE_MANAGER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>
#include "database_decoder.h"

template <class T>
class SingletonDecodeManager
{
public:
        template <typename... Args>

        static T* getInstance()
        {
                if(!decodeMngInstance_)
                {
                        decodeMngInstance_ = new T();
                }
                return decodeMngInstance_;
        }

        static void destroyInstance()
        {
                delete decodeMngInstance_;
                decodeMngInstance_ = nullptr;
        }

private:
        static T* decodeMngInstance_;
};

template <class T> T*  SingletonDecodeManager<T>::decodeMngInstance_ = nullptr;
class DecodeManager: public SingletonDecodeManager<DecodeManager>
{
public:
	void addDecoder(DatabaseDecoderPtr bbdddec) { decoders_.push_back(bbdddec);}
	void setDefaultDecoder(DatabaseDecoderPtr bbdddec) { def_decoder_ = bbdddec;}

	void statistics();
	DatabaseDecoderPtr getDefaultDecoder() { return def_decoder_;};
	friend class SingletonDecodeManager<DecodeManager>;
private:
	std::vector<DatabaseDecoderPtr> decoders_;
	DatabaseDecoderPtr def_decoder_;
};

#endif // FIRESQL_DECODE_MANAGER_H

